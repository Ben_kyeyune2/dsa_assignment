#include <stdio.h>
#include <math.h>

// Function to calculate the discriminant
double calculateDiscriminant(double a, double b, double c) {
    return b * b - 4 * a * c;
}

// Function to find and display roots
void findAndDisplayRoots(double a, double b, double c) {
    double discriminant = calculateDiscriminant(a, b, c);

    if (discriminant > 0) {
        // Real and distinct roots
        double root1 = (-b + sqrt(discriminant)) / (2 * a);
        double root2 = (-b - sqrt(discriminant)) / (2 * a);
        printf("Roots are real and distinct: %.2lf, %.2lf\n", root1, root2);
    } else if (discriminant == 0) {
        // Real and equal roots
        double root = -b / (2 * a);
        printf("Roots are real and equal: %.2lf\n", root);
    } else {
        // Complex roots
        double realPart = -b / (2 * a);
        double imagPart = sqrt(-discriminant) / (2 * a);
        printf("Roots are complex: %.2lf + %.2lfi, %.2lf - %.2lfi\n", realPart, imagPart, realPart, imagPart);
    }
}

int main() {
    double a, b, c;

    // Input coefficients
    printf("Enter coefficient a: ");
    scanf("%lf", &a);

    // Validate input for a not being zero
    if (a == 0) {
        printf("Coefficient 'a' cannot be zero. Exiting...\n");
        return 1;
    }

    printf("Enter coefficient b: ");
    scanf("%lf", &b);

    printf("Enter coefficient c: ");
    scanf("%lf", &c);

    // Find and display roots
    findAndDisplayRoots(a, b, c);

    return 0;
}
