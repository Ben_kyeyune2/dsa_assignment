//KYEYUNE BEN 2100705065 21/U/05065/PS


#include <stdio.h>
#include <stdlib.h>

// Structure definition for a Node
struct Node
{
    int number;
    struct Node *next;
};

// Function prototypes
struct Node *createNode(int num);
void printList(struct Node *head);
void append(struct Node **head, int num);
void prepend(struct Node **head, int num);
void deleteByKey(struct Node **head, int key);
void deleteByValue(struct Node **head, int value);
void insertAfterKey(struct Node **head, int key, int value);
void insertAfterValue(struct Node **head, int searchValue, int newValue);

int main()
{
    struct Node *head = NULL;
    int choice, data, key, searchValue;

    while (1)
    {
        printf("\nLinked Lists\n");
        printf("1. Print List\n");
        printf("2. Append\n");
        printf("3. Prepend\n");
        printf("4. Delete by Key\n");
        printf("5. Delete by Value\n");
        printf("6. Insert After Key\n");
        printf("7. Insert After Value\n");
        printf("8. Exit\n");
        printf("Enter your choice: ");
        scanf("%d", &choice);
        
        
        if (choice == 1)
        {
            printList(head);
        }
        else if (choice == 2)
        {
            printf("Enter data to append: ");
            scanf("%d", &data);
            append(&head, data);
        }
        else if (choice == 3)
        {
            printf("Enter data to prepend: ");
            scanf("%d", &data);
            prepend(&head, data);
        }
        else if (choice == 4)
        {
            printf("Enter key to delete: ");
            scanf("%d", &key);
            deleteByKey(&head, key);
        }
        else if (choice == 5)
        {
            printf("Enter value to delete: ");
            scanf("%d", &data);
            deleteByValue(&head, data);
        }
        else if (choice == 6)
        {
            printf("Enter key after which to insert: ");
            scanf("%d", &key);
            printf("Enter value to insert: ");
            scanf("%d", &data);
            insertAfterKey(&head, key, data);
        }
        else if (choice == 7)
        {
            printf("Enter value after which to insert: ");
            scanf("%d", &searchValue);
            printf("Enter value to insert: ");
            scanf("%d", &data);
            insertAfterValue(&head, searchValue, data);
        }
        else if (choice == 8)
        {
            printf("Exiting program.\n");
            exit(0);
        }
        else
        {
            printf("Invalid choice. Please try again.\n");
        }
    }

    return 0;
}

// Implement the function prototypes below this point

// Function to create a new node with given data
struct Node *createNode(int num)
{
    struct Node *newNode = (struct Node *)malloc(sizeof(struct Node));
    if (newNode == NULL)
    {
        printf("Error: Unable to allocate memory for a new node\n");
        exit(1);
    }

    newNode->number = num;
    newNode->next = NULL;
    return newNode;
}

// Function to print all nodes in the list
void printList(struct Node *head)
{
    struct Node *current = head;
    printf("[");
    while (current != NULL)
    {
        printf("%d", current->number);
        current = current->next;

        if (current != NULL)
        {
            printf(", ");
        }
    }
    printf("]\n");
}

// Function to add a node at the end of the list
void append(struct Node **head, int num)
{
    struct Node *newNode = createNode(num);
    if (*head == NULL)
    {
        *head = newNode;
        return;
    }
    struct Node *current = *head;
    while (current->next != NULL)
    {
        current = current->next;
    }
    current->next = newNode;
}

// Function to add a node at the beginning of the list
void prepend(struct Node **head, int num)
{
    struct Node *newNode = createNode(num);
    newNode->next = *head;
    *head = newNode;
}

// Function to delete a node by key
void deleteByKey(struct Node **head, int key)
{
    struct Node *current = *head;
    struct Node *prev = NULL;

    // Check if the key is in the first node
    if (current != NULL && current->number == key)
    {
        *head = current->next;
        free(current);
        printf("Node with key %d deleted.\n", key);
        return;
    }

    // Traverse the list to find the key
    while (current != NULL && current->number != key)
    {
        prev = current;
        current = current->next;
    }

    // If key was not present in the list
    if (current == NULL)
    {
        printf("Key %d not found in the list. Unable to delete.\n", key);
        return;
    }

    // Unlink the node from the linked list
    prev->next = current->next;
    free(current);
    printf("Node with key %d deleted.\n", key);
}

// Function to delete a node by value
void deleteByValue(struct Node **head, int value)
{
    struct Node *current = *head;
    struct Node *prev = NULL;

    // Check if the value is in the first node
    if (current != NULL && current->number == value)
    {
        *head = current->next;
        free(current);
        printf("Node with value %d deleted.\n", value);
        return;
    }

    // Traverse the list to find the value
    while (current != NULL && current->number != value)
    {
        prev = current;
        current = current->next;
    }

    // If value was not present in the list
    if (current == NULL)
    {
        printf("Value %d not found in the list. Unable to delete.\n", value);
        return;
    }

    // Unlink the node from the linked list
    prev->next = current->next;
    free(current);
    printf("Node with value %d deleted.\n", value);
}

// Function to insert a node after a given key
void insertAfterKey(struct Node **head, int key, int value)
{
    struct Node *current = *head;

    // Find the key in the list
    while (current != NULL && current->number != key)
    {
        current = current->next;
    }

    // If key was not present in the list
    if (current == NULL)
    {
        printf("Key %d not found in the list. Unable to insert.\n", key);
        return;
    }

    // Create a new node with the given value
    struct Node *newNode = createNode(value);

    // Insert the new node after the node with the given key
    newNode->next = current->next;
    current->next = newNode;
    printf("Node with value %d inserted after key %d.\n", value, key);
}

// Function to insert a node after a given value
void insertAfterValue(struct Node **head, int searchValue, int newValue)
{
    struct Node *current = *head;

    // Find the value in the list
    while (current != NULL && current->number != searchValue)
    {
        current = current->next;
    }

    // If value was not present in the list
    if (current == NULL)
    {
        printf("Value %d not found in the list. Unable to insert.\n", searchValue);
        return;
    }

    // Create a new node with the given value
    struct Node *newNode = createNode(newValue);

    // Insert the new node after the node with the given value
    newNode->next = current->next;
    current->next = newNode;
    printf("Node with value %d inserted after value %d.\n", newValue, searchValue);
}
